This repo is for the artifacts evaluation materials for paper ICSE-main-273.

# use the docker

```
$ tar -xzvf panorama-docker-image.tar.gz
$ docker load < panorama-docker-image.tar

```
follow the [instructions](https://hyperloop-rails.github.io/panorama/docs/docker)

# user study data

questionnaire: user-study/questionnaire.pdf

de-identified raw data: user-study/panorama-raw-data